

import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: HomeView,
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },
    {
      path: '/about',
      name: 'about',

      components: {
        default: () => import('../views/AboutView.vue'),
        menu: () => import('@/components/menus/AboutMenu.vue'),
        header: () => import('@/components/headers/AboutHeader.vue'),
      },
      meta: {
        layout: 'FullLayout',
      }
    },
    {
      path: '/pos',
      name: 'pos',
      components: {
        default: () => import('../views/PointOfSellView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },
    {
      path: '/product',
      name: 'product',
      components: {
        default: () => import('../views/ProductView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },
    {
      path: '/user',
      name: 'user',
      components: {
        default: () => import('../views/UserView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },
    
    {
      path: '/login',
      name: 'login',
      components: {
        default: () => import('../views/LoginView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'FullLayout',
        
      }
    },
    {
      path: '/customer',
      name: 'customer',
      components: {
        default: () => import('../views/CustomerView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },
    {
      path: '/order',
      name: 'order',
      components: {
        default: () => import('../views/OrderView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },
    {
      path: '/employee',
      name: 'employee',
      components: {
        default: () => import('../views/EmployeeView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },    {
      path: '/material',
      name: 'material',
      components: {
        default: () => import('../views/MaterialView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },
  ]
})
function isLogin() {
  const user = localStorage.getItem("email");
  
  console.log(user)
    if (user) {
      return true;
    }
    return false;
}
router.beforeEach((to, from) => {
  // instead of having to check every route record with
  // to.matched.some(record => record.meta.requiresAuth)
  console.log(isLogin())
  if (to.meta.requiresAuth && !isLogin()) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    
    return {
      path: '/login',
      // save the location we were at to come back later
      query: { redirect: to.fullPath },
    }
  }
})

export default router
