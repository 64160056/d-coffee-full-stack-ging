import type Customer from "@/types/Customer";
import http from "./axios";

function getCustomer() {
  return http.get("/customers");
}
function saveCustomer(customer: Customer & {files:File[]}) {
  const formDate = new FormData();
  formDate.append("name",customer.name);
  formDate.append("tel",customer.tel)
  formDate.append("point",`${customer.point}`)
  formDate.append("file",customer.files[0])
  
  return http.post("/customers", formDate,{headers: {
    "Content-Type" : "multipart/form-data"
  }});
}

function updateCustomerImg(id: number,files:File[]){
  const formDate = new FormData();
  formDate.append("file",files[0])
  return http.patch(`/customers/${id}/image`, formDate,{headers: {
    "Content-Type" : "multipart/form-data"
  }});
}

function updateCustomer(id: number, customer: Customer) {
  return http.patch(`/customers/${id}`, customer);
}
function deleteCustomer(id: number) {
  return http.delete(`/customers/${id}`);
}
export default { getCustomer, saveCustomer, updateCustomer, deleteCustomer,updateCustomerImg };