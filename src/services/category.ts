import type Category from "@/types/Category";
import http from "./axios";

function getCategory() {
  return http.get("/categories");
}
function saveCategory(Category: Category) {
  return http.post("/categories", Category);
}
function updateCategory(id: number, Category: Category) {
  return http.patch(`/categories/${id}`, Category);
}
function deleteCategory(id: number) {
  return http.delete(`/categories/${id}`);
}
export default { getCategory, saveCategory, updateCategory, deleteCategory };