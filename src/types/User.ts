export default interface User {
    id?: number;
    name: string;
    tel: string;
    email: string;
    position: string;
    password?: string;
    image?:string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
  }