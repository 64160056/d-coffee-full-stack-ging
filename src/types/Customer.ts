export default interface Customer {
    id?: number;
    name: string;
    tel: string;
    point: number;
    image?:string
    startdate: string;
    createdDate?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
  }