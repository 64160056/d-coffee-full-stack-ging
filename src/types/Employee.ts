import type User from "./User";

export default interface Employee {
    id?: number;
    name: string;
    address: string;
    tel: string;
    email: string;
    user?:User;
    position: string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
  }