import { ref, computed,} from 'vue'
import { defineStore } from 'pinia'
import type Customer from '@/types/Customer'
import CustomerService from "@/services/customer";

export const useCustomerStore = defineStore('Customer', () => {
  const dialog = ref(false);
  const Customers = ref<Customer[]>([]);
  const editedCustomer  = ref<Customer & { files: File[] } >({name:"", tel:"",point: 0, startdate:"" ,files:[]});
  async function GetCustomers(){
    try{
      const res = await CustomerService.getCustomer();
      Customers.value = res.data;
    }catch(e){
      console.log(e);
    }
  }
  async function saveCustomer(){
    try {
      if(editedCustomer.value.id) {
        const res = await CustomerService.updateCustomer(editedCustomer.value.id, editedCustomer.value);
      }else {
        const res = await CustomerService.saveCustomer(editedCustomer.value);
      }

      dialog.value = false;
      await GetCustomers();
    } catch(e) {
      console.log(e);
    }
  }
  async function deleteCustomer(id:number) {
    try {

        const res = await CustomerService.deleteCustomer(id);

      await GetCustomers();
    } catch(e) {
      console.log(e);
    }
  }
  function editCustomer(customer: Customer) {
    editedCustomer.value = JSON.parse(JSON.stringify(customer));
    dialog.value = true;
  }


  return { GetCustomers,editedCustomer, Customers, dialog, editCustomer, saveCustomer, deleteCustomer }
})