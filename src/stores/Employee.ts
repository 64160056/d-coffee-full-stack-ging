import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import EmployeeService from '@/services/employee'
import type Employee from '@/types/Employee'
import { useUserStore } from './User'

export const useEmployeeStore = defineStore('Employee', () => {
  const UserStore = useUserStore();
    const Employee = ref<Employee[]>([]);
    const dialog = ref(false);
    const editedEmployee = ref<Employee>({ name: "", email: "", address: "",tel: "", position:"" });
    const userId = ref<number>();

    watch(dialog, (newDialog) => {
        
        if (!newDialog) {
          editedEmployee.value = { name: "", email: "", address: "",tel:"", position:""  };
          userId.value =0;
        }
      });
    watch(userId, (newuserId) => {
      if(newuserId){
          const index = UserStore.UserIsNotEmp.findIndex((item) => item.id === userId.value);
         const email = UserStore.UserIsNotEmp[index].email;
         const name = UserStore.UserIsNotEmp[index].name;

         editedEmployee.value = { name: name, email: email, address: "",tel:"", position:""  }
         
      }
    })


      

    
    async function GetEmployee(){
        try {
            const res = await EmployeeService.getEmployee();
            Employee.value = res.data
        }catch(e){
            console.log(e);
        }
    }
    async function saveEmployee() {
        console.log(editedEmployee.value)
        try {
          if (editedEmployee.value.id) {

            const res = await EmployeeService.updateEmployee(
              editedEmployee.value.id,
              editedEmployee.value
            );
          } else {
            const res = await EmployeeService.saveEmployee(editedEmployee.value);
          }
    
          dialog.value = false;
          await GetEmployee();
        } catch (e) {
          
          console.log(e);
        }
    
      }
    
      async function deleteEmployee(id: number) {
       
        try {
          const res = await EmployeeService.deleteEmployee(id);
          await GetEmployee();
          await UserStore.GetUserNotEmp();
        } catch (e) {
          console.log(e);
          
        }
       
      }
      function editEmployee(employee: Employee) {
        editedEmployee.value = JSON.parse(JSON.stringify(employee));
        dialog.value = true;
      }
    


  return { GetEmployee,Employee,editEmployee,deleteEmployee,saveEmployee,dialog,editedEmployee,userId}
})