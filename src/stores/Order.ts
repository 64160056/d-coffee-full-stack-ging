import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type OrderItem from '@/types/OrderItem'
import type Product from '@/types/Product'
import type Order from '@/types/Order'
import OrderService from '@/services/Order'

export const useOrderStore = defineStore('Order', () => {
    const OrderItem = ref<OrderItem[]>([]);
    const Bill = ref<OrderItem[]>([]);
    const dialog = ref(false);                                                      
    const member = ref(null)
    const model = ref(false);
    const total = ref(0)
    
    watch((dialog),(newDialog) => {
        if(!newDialog){
            model.value = false
            member.value = null
            
        }
    })


    function addOrderItem(item:Product){
        const index =  OrderItem.value.findIndex((orderItems) => orderItems.productId=== item.id);
        console.log(index);
        if(index === -1){
            Bill.value.push({productId:item.id!,product:item,price:item.price,amount:1,total:item.price,})
            //
            OrderItem.value.push({ productId:item.id! ,amount:1})

            //
            total.value = total.value + item.price 
            
            
        }else{
            console.log(OrderItem)
            OrderItem.value[index].amount++
            // 
            Bill.value[index].amount++
            Bill.value[index].total = Bill.value[index].amount * Bill.value[index].price!; 

            //
            total.value = total.value + item.price;
        }

    }
    function RemoveOrderItem(item:OrderItem){
        const index =  OrderItem.value.findIndex((orderItems) => orderItems.productId=== item.productId);
        if(OrderItem.value[index].amount > 1){
            OrderItem.value[index].amount--
            
            // 
            Bill.value[index].amount--
            Bill.value[index].total = Bill.value[index].amount * Bill.value[index].price!; 

            total.value = total.value - item.price!;
        }else{
            btnRemoveOrderItem(item)
        }

    }

    function btnRemoveOrderItem(item:OrderItem){
        const index =  OrderItem.value.findIndex((orderItems) => orderItems.productId=== item.productId);
         total.value = total.value - Bill.value[index].total!;
        OrderItem.value.splice(index,1)
        Bill.value.splice(index,1)
        
    }
    async function submitOrder(Id?:number){

        const Order = ref<Order>({userId:Id!,orderItems:OrderItem.value})
        const res = await OrderService.saveOrder(Order.value);
        total.value = 0;
        Bill.value = [];
        OrderItem.value = [];
        dialog.value = false
    }

  return {OrderItem,addOrderItem ,btnRemoveOrderItem,RemoveOrderItem ,Bill, submitOrder,dialog,member,model,total}
})