import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type User from '@/types/User'
import UserService from '@/services/user'

export const useUserStore = defineStore('User', () => {
  const User = ref<User[]>([]);
  const dialog = ref(false);
  const editedUser = ref<User & { files: File[] }>({ name: "", email: "", tel: "", position: "", image: "no_image_available.jpg",files:[] });
  const UserIsNotEmp = ref<User[]>([])
  watch(dialog, (newDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedUser.value = { name: "", email: "", tel: "", position: "", image: "no_image_available.jpg",files:[] };
    }
  });
  async function GetUserNotEmp() {
    try {
      const res = await UserService.getUserIsNotEmp();
      UserIsNotEmp.value = res.data
    } catch (e) {
      console.log(e);
    }
  }


  async function GetUser() {
    try {
      const res = await UserService.getUser();
      User.value = res.data
    } catch (e) {
      console.log(e);
    }
  }
  async function saveUser() {
    console.log(editedUser.value)
    try {
      if (editedUser.value.id) {

        const res = await UserService.updateUser(
          editedUser.value.id,
          editedUser.value
        );
      } else {
        const res = await UserService.saveUser(editedUser.value);
      }

      dialog.value = false;
      await GetUser();
    } catch (e) {

      console.log(e);
    }

  }

  async function deleteUser(id: number) {

    try {
      const res = await UserService.deleteUser(id);
      await GetUser();
    } catch (e) {
      console.log(e);

    }

  }
  function editUser(user: User) {
    editedUser.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  }



  return { GetUser, User, editUser, deleteUser, saveUser, dialog, editedUser,GetUserNotEmp,UserIsNotEmp }
})