import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type Product from '@/types/Product'
import ProductService from "@/services/product";

export const useProductStore = defineStore('Product', () => {
  const dialog = ref(false);
  const Products = ref<Product[]>([]);

  const ProductsByCate = ref<Product[]>([]);
  const editedProduct  = ref<Product & { files: File[] }>({name:"", price:0, amount:0, image: "no_image_available.jpg",categoryId:0,files:[]});

  

  watch([dialog], ([newDialog], oldDialog)=>{
    if(!newDialog ) {
      editedProduct.value = {name:"", price:0, amount:0, image: "no_image_available.jpg",categoryId:0,files:[]};
    }
 
  });
  async function GetProductsByCategory(id:number){
    try{
      const res = await ProductService.GetProductsByCategory(id)
      ProductsByCate.value = res.data
    } catch(e){
      console.log(e)

    }
  }

  async function GetProducts(){
    
    try{
      const res = await ProductService.getProduct();
      Products.value = res.data;
    }catch(e){
      console.log(e);
    }
  }

  async function saveProduct(){
    console.log(editedProduct.value)
    try {
      if(editedProduct.value.id) {
       
        const res = await ProductService.updateProduct(editedProduct.value.id, editedProduct.value);
        
      }else {
        const res = await ProductService.saveProduct(editedProduct.value);
      }

      dialog.value = false;
      await GetProducts();
    } catch(e) {
      console.log(e);
    }
  }

  async function deleteProduct(id:number) {
    try {
     
        const res = await ProductService.deleteProduct(id);

      await GetProducts();
    } catch(e) {
      console.log(e);
    }
  }
  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }

  return { GetProducts ,Products, dialog, editedProduct, saveProduct, editProduct, deleteProduct,GetProductsByCategory,ProductsByCate }
})
