import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type Material from '@/types/material'
import MaterialService from '@/services/material'
import { watch } from 'vue'
export const useMaterialStore = defineStore('Material', () => {
  const Material = ref<Material[]>([]);
  const editedMaterial = ref<Material>({name:"",min_quantity:0,price_per_unit:0,quantity:0,unit:''})
  const dialog = ref(false)

  watch(dialog, newDailog => {
    if(!newDailog){
        editedMaterial.value = {name:"",min_quantity:0,price_per_unit:0,quantity:0,unit:''}
    }
  })

  async function getMaterial(){
   try{
    const res = await MaterialService.getMaterial();
    Material.value = res.data;

   }catch(e){
    console.log(e)
   }

  }

  async function saveMaterial(){
    try{
       const res = await MaterialService.saveMaterial(editedMaterial.value)
       
    } catch(e){
        console.log(e)
    }
    await getMaterial();
    dialog.value = false
  }
  async function deleteProduct(id:number) {
    try {
     
        const res = await MaterialService.deleteMaterial(id);

      await getMaterial();
    } catch(e) {
      console.log(e);
    }
  }

  return { Material,getMaterial,dialog,editedMaterial ,saveMaterial,deleteProduct}
})
