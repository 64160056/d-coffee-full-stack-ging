import { ref, computed } from "vue";
import { defineStore } from "pinia";
import auth from "@/services/auth";
import router from "@/router";
import { useMessageStore } from "./message";


export const useAuthStore = defineStore("auth", () => {
  const authName = ref("");
  const messageStore = useMessageStore();

  const isLogin = () => {
    const user = localStorage.getItem("email");
    if (user) {
      return true;
    }
    return false;
  };
  const login = async (username: string, password: string): Promise<void> => {
    //loadingStore.isLoading = true;
    try {
      
      const res = await auth.login(username, password);
      console.log(res);
      localStorage.setItem("username", res.data.user.name);
      localStorage.setItem("email", res.data.user.email);
      localStorage.setItem("token", res.data.access_token);
      localStorage.setItem("img", res.data.user.image);
      router.push("/");
    } catch (e) {
      messageStore.showError("Username หรือ Passwprd ไม่ถูกต้อง");
    }
    //loadingStore.isLoading = false;
    //localStorage.setItem("token", userName);
  };
  const logout = () => {
    //authName.value = "";
    localStorage.removeItem("username");
    localStorage.removeItem("email");
    localStorage.removeItem("token");
    localStorage.removeItem("img")
    router.replace("/login");
  };

  return { login, logout, isLogin };
});
